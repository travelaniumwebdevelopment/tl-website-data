(function($, window) {
  const WebsiteData = () => {
    return {
      post(formobj, options) {
        var DATA = $.extend({}, {
          formKey: '',
          name: '',
          phone: '',
          email: '',
          countryCode: '',
          countryName: '',
          message: ''
        }, formobj);
      
        var OPTS = $.extend({}, {
          success: function() {},
          fail: function() {},
          always: function() {}
        }, options);
      
        var POSTDATA = $.post({
          url: 'https://hotelservices.travelanium.net/crs-customer-form-rs/restresources/CRSCustomerFormDataService',
          contentType: 'application/json',
          data: JSON.stringify(DATA)
        });
      
        POSTDATA.done((res, status, xhr) => {
          OPTS.success(res, status, xhr);
        });
      
        POSTDATA.fail((xhr, status, res) => {
          OPTS.fail(xhr, status, res);
        });
      
        POSTDATA.always(() => {
          OPTS.always();
        });

        return DATA;
      }
    }
  }

  if (window.WebsiteData === undefined) {
    window.WebsiteData = WebsiteData;
  }
})(jQuery, window);