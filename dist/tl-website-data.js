"use strict";

(function ($, window) {
  var WebsiteData = function WebsiteData() {
    return {
      post: function post(formobj, options) {
        var DATA = $.extend({}, {
          formKey: '',
          name: '',
          phone: '',
          email: '',
          countryCode: '',
          countryName: '',
          message: ''
        }, formobj);
        var OPTS = $.extend({}, {
          success: function success() {},
          fail: function fail() {},
          always: function always() {}
        }, options);
        var POSTDATA = $.post({
          url: 'https://hotelservices.travelanium.net/crs-customer-form-rs/restresources/CRSCustomerFormDataService',
          contentType: 'application/json',
          data: JSON.stringify(DATA)
        });
        POSTDATA.done(function (res, status, xhr) {
          OPTS.success(res, status, xhr);
        });
        POSTDATA.fail(function (xhr, status, res) {
          OPTS.fail(xhr, status, res);
        });
        POSTDATA.always(function () {
          OPTS.always();
        });
        return DATA;
      }
    };
  };

  if (window.WebsiteData === undefined) {
    window.WebsiteData = WebsiteData;
  }
})(jQuery, window);
//# sourceMappingURL=tl-website-data.js.map