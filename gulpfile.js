var {task, watch, src, dest, parallel, series} = require('gulp');
var babel = require('gulp-babel');
var minify = require('gulp-minify');

task('script', () => {
  return src('./src/tl-website-data.js', {sourcemaps: true})
    .pipe(babel())
    .pipe(dest('./dist', {sourcemaps: '.'}))
})
  
task('script-minify', () => {
  return src('./src/tl-website-data.js', {sourcemaps: true})
    .pipe(babel())
    .pipe(minify({
      ext: {
        min: '.min.js'
      }
    }))
    .pipe(dest('./dist', {sourcemaps: '.'}))
})

task('watch:code', () => {
  watch( './src/*.js', parallel('script', 'script-minify') );
})

task('compile', parallel('script', 'script-minify'));
task('default', parallel('compile', 'watch:code'))