# Travelanium Website Data

ปลั๊กอิน Website Data จะเป็นตัวช่วยในการประสานการรับ-ส่งข้อมูลระหว่าง Travelanium และ Website

## Dependency

- jQuery

## Methods

```post( object $formdata, object $options)```

ใช้สำหรับส่งข้อมูลเข้าไปยัง Travelanium Form API

$formdata
> (object) (Required) ข้อมูลจากฟอร์มในรูปแบบ Object

$options
> (object) (Optional) การตั้งค่าต่าง ๆ 

## Usage

```javascript
// Get instances
const WD = new WebsiteData();

// All parameters
var $formdata = {
  formKey: '',
  name: '',
  phone: '',
  email: '',
  countryCode: '',
  countryName: '',
  message: ''
}

// Callbacks after ajax process
var $options = {
  done: function(res, status, xhr) {},  // รันฟังก์ชันเมื่อฟอร์มส่งสำเร็จ
  fail: function(xhr, status, res) {},  // รันฟังก์ชันเมื่อฟอร์มมีข้อผิดพลาด
  always: function() {}                 // รันฟังก์ชันเสมอ
}

// Run script
WD.post( $formdata, $options );
```

## Real-World Example
```html
<html>
  <head>
    ...
  </head>

  <body>
    ...

    <script src="jquery.js"></script>
    <script src="tl-website-data.min.js"></script>
    <script>
      (function($) {
        var WD = new WebsiteData();

        $('#your_form_id').on('submit', function(event) {
          event.preventDefault();

          var formdata = {
            formKey: 'xxxxxxxxxxxxxxxxxxxxxxxx', //THIS IS API, required
            email: $(this).find('[name=cemail]').val(), //optional
            name: '', //optional
            phone: '', //optional
            countryCode: '', //optional
            countryName: '', //optional
            message: '' //optional
          }

          WD.post( formdata, {
            done: function() {
              // ถ้าส่งสำเร็จ ให้รัน script ส่วนนี้ต่อ
              // เช่น แจ้งเตือนว่าสมัครสำเร็จแล้ว

              alert('Thanks for your subscription!');
            },
            fail: function(xhr) {
              // ถ้าส่งไม่สำเร็จ ให้รัน script ส่วนนี้ต่อ
              // เช่น แจ้งเตือนผู้ใช้

              alert('Sorry, something went wrong');
              console.log(xhr);
            },
            always: function() {
              // รัน script ส่วนนี้เสมอไม่ว่าจะส่งข้อมูลสำเร็จหรือไม่ก็ตาม
            }
          } );
        });
      })(jQuery);
    </script>
  </body>
</html>
```